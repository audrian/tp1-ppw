from django.apps import AppConfig


class StudentunionNewsConfig(AppConfig):
    name = 'studentunion_news'
