from django.db import models
from datetime import datetime

# Create your models here.
class aNews(models.Model):
    tittle = models.CharField(max_length = 100)
    date = models.DateField(default=datetime.now, blank=True)
    content = models.CharField(max_length = 300)
