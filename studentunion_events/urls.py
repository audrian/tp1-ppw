from django.urls import path
from . import views

urlpatterns = [
	path('detail/<id>', views.detail, name='detail'),
	path('enter/<id>', views.enter, name='enter'),
	path('', views.index, name='events'),
]