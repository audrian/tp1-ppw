from django.apps import AppConfig


class StudentunionEventsConfig(AppConfig):
    name = 'studentunion_events'
