from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from django.core.exceptions import ValidationError

from .views import index, detail
from .models import Event
from studentunion_register.models import Member

import datetime

# Create your tests here.
class StudentUnionEventsTest(TestCase):
	def setUp(self):
		Event.objects.create(
			name = "Sample event",
			location = "Sample location",
			description = "A description",
			category = "A category",
			date = datetime.date(2018, 12, 14),
			start_time = datetime.time(10, 0),
			end_time = datetime.time(18, 0)
		)

	def test_events_page_exists(self):
		response = Client().get('/events/')
		self.assertEqual(response.status_code, 200)

	def test_events_uses_correct_template(self):
		found = resolve('/events/')
		self.assertEqual(found.func, index)

	def test_details_page_exists(self):
		response = Client().get('/events/detail/1')
		self.assertEqual(response.status_code, 200)

	def test_details_page_uses_correct_function(self):
		found = resolve('/events/detail/1')
		self.assertEqual(found.func, detail)

	def test_events_create(self):
		events_count = Event.objects.count()

		new_event = Event.objects.create(
			name = "Test event",
			location = "Test location",
			description = "Test description",
			category = "Test category",
			date = datetime.date(2018, 12, 13),
			start_time = datetime.time(12, 0),
			end_time = datetime.time(13, 0),
		)

		self.assertEqual(Event.objects.count(), events_count + 1)

	def test_events_create_incorrect_time(self):
		new_event = Event.objects.create(
			name = "Test event",
			location = "Test location",
			description = "Test description",
			category = "Test category",
			date = datetime.date(2018, 12, 13),
			start_time = datetime.time(13, 0),
			end_time = datetime.time(12, 0),
		)

		self.assertRaises(ValidationError, new_event.clean)

	def test_events_enter(self):
		new_event = Event.objects.create(
			name = "Test event",
			location = "Test location",
			description = "Test description",
			category = "Test category",
			date = datetime.date(2018, 12, 13),
			start_time = datetime.time(12, 0),
			end_time = datetime.time(13, 0),
		)

		new_member = Member.objects.create(
			name = "Test User",
			username = "test.user",
			email = "test.user@example.com",
			dob = datetime.date(1999, 4, 9),
			address = "Nowhereland"
		)

		new_event.entrants.add(new_member)

		self.assertEqual(new_event.entrants.count(), 1)

	def test_events_enter_already_entered_member(self):
		new_event = Event.objects.create(
			name = "Test event",
			location = "Test location",
			description = "Test description",
			category = "Test category",
			date = datetime.date(2018, 12, 13),
			start_time = datetime.time(12, 0),
			end_time = datetime.time(13, 0),
		)

		new_member = Member.objects.create(
			name = "Test User",
			username = "test.user",
			email = "test.user@example.com",
			dob = datetime.date(1999, 4, 9),
			address = "Nowhereland"
		)

		new_event.entrants.add(new_member)
		new_event.entrants.add(new_member)

		self.assertEqual(new_event.entrants.count(), 1)

