from django.test import TestCase, Client
from django.urls import resolve
from .views import home
#from .models import Home

# Create your tests here.
class StudentUnionHomeTest(TestCase):
    def test_home_page_exists(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 301)

    def test_home_using_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'home.html')

    def test_home_using_correct_template(self):
        found = resolve('/')
        self.assertEqual(found.func, home) 
        
    def test_landingpage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_home_using_base_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'base.html')

    def test_home_displays_upcoming_events(self):
        pass

    def test_home_displays_recent_news(self):
        pass

