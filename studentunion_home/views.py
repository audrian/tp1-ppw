from django.shortcuts import render
#from .models import Home
from studentunion_events.models import Event
from studentunion_news.models import aNews

# Create your views here.
def home(request) :
    context = {        
        "events" : Event.objects.order_by('-date')[:3],
        "news" : aNews.objects.order_by('-date')[:3],
    }
    return render(request, 'home.html', context)

