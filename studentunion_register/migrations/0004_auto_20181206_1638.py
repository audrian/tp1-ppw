# Generated by Django 2.1.3 on 2018-12-06 09:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('studentunion_register', '0003_auto_20181018_1615'),
    ]

    operations = [
        migrations.RenameField(
            model_name='member',
            old_name='Address',
            new_name='address',
        ),
        migrations.RenameField(
            model_name='member',
            old_name='DOB',
            new_name='dob',
        ),
        migrations.RenameField(
            model_name='member',
            old_name='Email',
            new_name='email',
        ),
        migrations.RenameField(
            model_name='member',
            old_name='Name',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='member',
            old_name='Password',
            new_name='password',
        ),
        migrations.RenameField(
            model_name='member',
            old_name='Username',
            new_name='username',
        ),
    ]
