from django.apps import AppConfig


class StudentunionRegisterConfig(AppConfig):
    name = 'studentunion_register'
