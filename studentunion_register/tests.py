from django.test import TestCase, Client
from django.urls import resolve
from .views import register
from .models import Member
from .forms import RegisterForm
import datetime

# Create your tests here.
class StudentUnionRegisterTest(TestCase):
    def test_register_page_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)
    
    def test_register_uses_correct_template(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    def test_register_form_valid(self):
        form = RegisterForm(data={ 'name' : 'Testy McTestFace', 
        'username' : 'testymctest',
        'email' : 'test.email@example.com',
        'dob': datetime.datetime.now().date(),
        'address' : 'Nowheretown, USA',
        'password1' : 'thisisjustatest',
        'password2' : 'thisisjustatest'
        })
        self.assertTrue(form.is_valid())

    def test_register_form_password_invalid(self):
        # Password confirmation fail
        form = RegisterForm(data={ 'name' : 'Testy McTestFace', 
        'username' : 'testymctest',
        'email' : 'test.email@example.com',
        'dob': datetime.datetime.now().date(),
        'address' : 'Nowheretown, USA',
        'password1' : 'thisisjustatest',
        'password2' : 'thisisjusta'
        })

        self.assertFalse(form.is_valid())

        # Password too short
        form = RegisterForm(data={ 'name' : 'Testy McTestFace', 
        'username' : 'testymctest',
        'email' : 'test.email@example.com',
        'dob': datetime.datetime.now().date(),
        'address' : 'Nowheretown, USA',
        'password1' : 'this',
        'password2' : 'this'
        })

        self.assertFalse(form.is_valid())

        # Password not alphanumeric
        form = RegisterForm(data={ 'name' : 'Testy McTestFace', 
        'username' : 'testymctest',
        'email' : 'test.email@example.com',
        'dob': datetime.datetime.now().date(),
        'address' : 'Nowheretown, USA',
        'password1' : '-!foo1234!-',
        'password2' : '-!foo1234!-'
        })

        self.assertFalse(form.is_valid())

    def test_register_create_member(self):
        new_member = Member.objects.create(
            name = "Testy McTestFace",
            username = "testy.mctestface",
            email = "test.email@example.com",
            dob = datetime.datetime.now().date(),
            address = "Nowheretown, USA"
        )

        self.assertTrue(Member.objects.count(), 1)

    def test_register_form_view(self):
        data = { 'name' : 'Testy McTestFace', 
        'username' : 'testymctest',
        'email' : 'test.email@example.com',
        'dob': datetime.datetime.now().date(),
        'address' : 'Nowheretown, USA',
        'password1' : 'thisisjustatest',
        'password2' : 'thisisjustatest'
        }
    
        response = self.client.post('/register/', data)
        # messages = list(response.context['messages'])
        
        self.assertEqual(response.status_code, 302)






